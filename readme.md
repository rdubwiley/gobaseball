# Gobaseball

## This project tries to replicate some of the functionality of pybaseball by pulling statcast data.

## This is a hobby project as a means to learn golang, cobra, and bubbletea (or more generally the charm ecosystem)

### The cli has two commands
    //This pulls down the full statcast file between two dates
    ./gobaseball get-statcast <save_loc> <start_date> <end_date>

    //This opens a local csv using the bubble table component
    ./gobaseball view-csv <file_loc> <numCols> <numRows>

### You can also use --help to see more of a description on how to use these.
### Note that you only want to pull a few days for get-statcast as there are limits.
### My general recommendation is to run a loop that runs them by each day for what you need.

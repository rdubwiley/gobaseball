package main

import (
	commands "gitlab.com/rdubwiley/gobaseball/commands"

	"github.com/spf13/cobra"
)

func main() {
	var rootCmd = &cobra.Command{Use: "app"}
	rootCmd.AddCommand(commands.GetStatcast, commands.ViewCsv)
	rootCmd.Execute()
}

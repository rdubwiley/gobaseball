package helpers

import (
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/go-gota/gota/dataframe"
)

func ReadCSVFromUrl(url string) (dataframe.DataFrame, error) {
	resp, err := http.Get(url)
	if err != nil {
		return dataframe.DataFrame{}, err
	}
	defer resp.Body.Close()
	df := dataframe.ReadCSV(resp.Body, dataframe.WithLazyQuotes(true))
	return df, df.Err
}

func SaveCSVStrings(loc string, data [][]string) {
	f, err := os.Create(loc)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	for _, line := range data {
		var linedata string = strings.Join(line, ",")
		_, err := f.WriteString(linedata + "\n")
		if err != nil {
			log.Fatal(err)
		}
	}
}

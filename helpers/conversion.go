package helpers

import "strconv"

func ToInt(str string) int {
	int, err := strconv.Atoi(str)
	if err != nil {
		panic(err)
	}
	return int
}

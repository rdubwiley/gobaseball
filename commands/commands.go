package commands

import "github.com/spf13/cobra"

var (
	GetStatcast = &cobra.Command{
		Use:   "get-statcast [loc] [startDate] [endDate]",
		Short: "Save the csv of statcast for a current date range",
		Long:  `Save the csv of statcast for a current date range`,
		Args:  cobra.MinimumNArgs(2),
		Run:   saveStatcast,
	}
	ViewCsv = &cobra.Command{
		Use:   "view-csv [loc] [numCols] [numRows]",
		Short: "Look at a local CSV",
		Long:  `Look at a local CSV`,
		Args:  cobra.MinimumNArgs(1),
		Run:   lookAtCsv,
	}
)

package commands

import (
	"fmt"
	"log"
	"os"
	helpers "gitlab.com/rdubwiley/gobaseball/helpers"

	"github.com/charmbracelet/bubbles/table"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/go-gota/gota/dataframe"
	"github.com/go-gota/gota/series"
	"github.com/spf13/cobra"
)

var baseStyle = lipgloss.NewStyle().
	BorderStyle(lipgloss.NormalBorder()).
	BorderForeground(lipgloss.Color("240"))

type model struct {
	table    table.Model
	df       dataframe.DataFrame
	colStart int
	colEnd   int
}

type lookConfig struct {
	fileLoc string
	numCols int
	numRows int
}

func NewLookConfig(args []string) lookConfig {
	switch argLength := len(args); {
	case argLength > 3:
		return lookConfig{}
	case argLength == 1:
		return lookConfig{args[0], 0, 0}
	case argLength == 2:
		return lookConfig{args[0], helpers.ToInt(args[1]), 0}
	default:
		return lookConfig{args[0], helpers.ToInt(args[1]), helpers.ToInt(args[2])}
	}
}

func (m model) Init() tea.Cmd { return nil }

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "esc":
			if m.table.Focused() {
				m.table.Blur()
			} else {
				m.table.Focus()
			}
		case "q", "ctrl+c":
			return m, tea.Quit
		case "right":
			if m.colEnd < len(m.df.Names()) {
				m.colEnd++
				m.colStart++
				m.table = m.updateTable()
			}
		case "left":
			if m.colStart != 0 {
				m.colEnd--
				m.colStart--
				m.table = m.updateTable()
			}

		}

	}

	m.table, cmd = m.table.Update(msg)
	return m, cmd
}

func (m model) View() string {
	return baseStyle.Render(m.table.View()) + "\n"
}

func (m model) updateTable() table.Model {
	newColumns, newRows := convertDataframe(m.df, m.colStart, m.colEnd)
	m.table.SetColumns(newColumns)
	m.table.SetRows(newRows)
	return m.table

}

// TODO implement rightward scrolling
func lookAtCsv(cmd *cobra.Command, args []string) {
	lookConfig := NewLookConfig(args)
	file, err := os.Open(lookConfig.fileLoc)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	df := dataframe.ReadCSV(file)
	numRows := 0
	numCols := 0
	if lookConfig.numRows == 0 {
		numRows = 10
	} else {
		numRows = lookConfig.numRows
	}
	if lookConfig.numCols == 0 {
		numCols = 10
	} else {
		numCols = lookConfig.numCols
	}
	firstColumns, firstRows := convertDataframe(df, 0, numCols)
	t := table.New(
		table.WithColumns(firstColumns),
		table.WithRows(firstRows),
		table.WithFocused(true),
		table.WithHeight(numRows),
	)
	s := table.DefaultStyles()
	s.Header = s.Header.
		BorderStyle(lipgloss.NormalBorder()).
		BorderForeground(lipgloss.Color("240")).
		BorderBottom(true).
		Bold(false)
	s.Selected = s.Selected.
		Foreground(lipgloss.Color("229")).
		Background(lipgloss.Color("57")).
		Bold(false)
	t.SetStyles(s)

	m := model{t, df, 0, numCols}
	if _, err := tea.NewProgram(m).Run(); err != nil {
		fmt.Println("Error running program:", err)
		os.Exit(1)
	}

}

func shrinkTable(columns []table.Column, rowsList []table.Row, nCols int, nRows int) ([]table.Column, []table.Row) {
	rows := rowsList[:nRows]
	var outTable []table.Row
	for _, row := range rows {
		outTable = append(outTable, row[:nCols])
	}
	return columns[:nCols], outTable
}

func convertDataframe(df dataframe.DataFrame, colStart int, colEnd int) ([]table.Column, []table.Row) {
	var columnData []series.Series
	colNames := df.Names()
	nrows := df.Nrow()
	var columns []table.Column
	for i, col := range colNames {
		if colStart <= i && i <= colEnd-1 {
			columnData = append(columnData, df.Col(col))
			columns = append(columns, table.Column{Title: col, Width: len(col)})
		}

	}
	var dataTable []table.Row
	var rowData table.Row
	for rowNum := range nrows {
		for i, cd := range columnData {
			elem := cd.Elem(rowNum)
			selem := fmt.Sprint(elem)
			if len(selem) > columns[i].Width {
				columns[i].Width = len(selem)
			}
			rowData = append(rowData, selem)
		}
		dataTable = append(dataTable, rowData)
		rowData = table.Row{}
	}
	return columns, dataTable
}

package commands

import (
	"fmt"
	"os"

	helpers "gitlab.com/rdubwiley/gobaseball/helpers"

	"github.com/spf13/cobra"
)

type statcastConfig struct {
	outputLoc string
	startDate string
	endDate   string
}

func NewStatcastConfig(args []string) statcastConfig {
	switch argLength := len(args); {
	case argLength > 3:
		return statcastConfig{}
	case argLength < 3:
		return statcastConfig{args[0], args[1], "2024-12-31"}
	default:
		return statcastConfig{args[0], args[1], args[2]}
	}
}

func (c statcastConfig) print() {
	fmt.Println(c.outputLoc)
	fmt.Println(c.startDate)
	fmt.Println(c.endDate)
}

func saveStatcast(cmd *cobra.Command, args []string) {
	config := NewStatcastConfig(args)
	var url = getUrl(config.startDate, config.endDate)
	df, err := helpers.ReadCSVFromUrl(url)
	if err != nil {
		panic(err)
	}
	file, err := os.Create(config.outputLoc)
	if err != nil {
		cmd.Println("Uh oh")
	}
	df.WriteCSV(file)
	fmt.Println()
	cmd.Println("Saved to %s", config.outputLoc)
}

func getUrl(startDate string, endDate string) string {
	var rawUrl string = "https://baseballsavant.mlb.com/statcast_search/csv?all=true&hfPT=&hfAB=&hfBBT=&hfPR=&hfZ=&stadium=&hfBBL=&hfNewZones=&hfGT=R|PO|S|=&hfSea=&hfSit=&player_type=pitcher&hfOuts=&opponent=&pitcher_throws=&batter_stands=&hfSA=&game_date_gt=%s&game_date_lt=%s&position=&hfRO=&home_road=&hfFlag=&metric_1=&hfInn=&min_pitches=0&min_results=0&group_by=name&sort_col=pitches&player_event_sort=h_launch_speed&sort_order=desc&min_abs=0&type=details"
	return fmt.Sprintf(rawUrl, startDate, endDate)
}
